#!/bin/sh

[ -d .git ] || git clone ${MKDOCS_SERVER_GIT_URL} /docs

git pull
pip install -r requirements.txt
mkdocs build -d /site
nginx&
python /hook/docker-hook -c sh /hook/update.sh
