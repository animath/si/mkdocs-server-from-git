FROM python:3-alpine

RUN apk --update add git less openssh nginx curl gcc libc-dev && \
    rm -rf /var/lib/apt/lists/* && \
    rm /var/cache/apk/*

RUN pip install requests mkdocs mkdocs-material --no-cache-dir

RUN mkdir /hook && mkdir /docs && mkdir /site

# Configure nginx
RUN mkdir /run/nginx
RUN ln -sf /dev/stdout /var/log/nginx/access.log && ln -sf /dev/stderr /var/log/nginx/error.log
COPY nginx.conf /etc/nginx/conf.d/mkdocs-server.conf
RUN rm /etc/nginx/conf.d/default.conf

COPY ./entrypoint.sh /hook
COPY ./docker-hook /hook
COPY ./update.sh /hook

WORKDIR /docs

RUN umask 0002

ENTRYPOINT ["/hook/entrypoint.sh"]

EXPOSE 80
