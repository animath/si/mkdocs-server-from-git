# Serveur Mkdocs auto-mis à jour via Git

Cette image Docker permet la mise en place d'un serveur Nginx dont les sources sont un site
écrit en Markdown et compilé grâce à Mkdocs.

À titre d'exemple, le site [organiser-un.tfjm.org](https://organiser-un.tfjm.org) est écrit
via ce procédé.

Cependant, cette image offre l'avantage de mettre à jour automatiquement le site dès lors
que la branche `master` du dépôt Git est modifiée.

## Configuration

On désignera par `www.exemple.com` le site sur lequel vous souhaitez héberger votre site.
Sur votre dépôt Git, ajoutez un nouveau webhook déclenché à chaque événement `push`
(cela suffit normalement) vers l'adresse `https://www.exemple.com/trigger-ci.json`.

Il vous suffit ensuite de lancer l'image Docker en renseignant la variable d'environnement
`MKDOCS_SERVER_GIT_URL` par l'adresse de clonage **HTTPS** du dépôt Git. Par exemple, pour
ce dépôt, il s'agit de l'adresse `https://gitlab.com/animath/si/mkdocs-server-from-git.git`.

Un exemple :

```bash
docker run ynerant/mkdocs-server-from-git -e MKDOCS_SERVER_GIT_URL=https://gitlab.com/me/mon-super-projet-mkdocs.git -p 8000:80
```

Le résultat s'observe sur `http://localhost:8000`.

Il est recommandé d'utiliser `docker-compose` :

```yaml
    mkdocs-server:
        image: ynerant/mkdocs-server-from-git
        ports:
            - 8000:80
        environment:
            - MKDOCS_SERVER_GIT_URL=https://gitlab.com/me/mon-super-projet.git 
```

Si vous utilisez Traefik comme reverse-proxy :

```yaml
    mkdocs-server:
        image: ynerant/mkdocs-server-from-git
        restart: always
        environment:
            - MKDOCS_SERVER_GIT_URL=https://gitlab.com/me/mon-super-projet.git
        labels:
            - "traefik.enable=true"
            - "traefik.http.routers.mkdocs-server.rule=Host(`www.exemple.com`)"
            - "traefik.http.routers.mkdocs-server.entrypoints=websecure"
            - "traefik.http.routers.mkdocs-server.tls.certresolver=mytlschallenge"
```

À vous bien sûr d'adapter la configuration comme bon vous semble.

Enjoy :)

## Fonctionnement

L'image contient un serveur Nginx avec deux entrées : un serveur classique desservant
le dossier `/site` et une entrée écoutant sur `/trigger-ci.json` écrite en Python
afin de récupérer l'événement de `push` envoyé par le serveur Git.

À chaque push, le script `update.sh` est exécuté, visant simplement à effectuer un
`git pull` afin de mettre à jour les fichiers, puis `mkdocs build /docs -d /site`
afin de compiler les fichiers Markdown présents dans `/docs` vers la destination
`/site`, lue par Nginx directement.

Il est à noter qu'un fichier `requirements.txt` doit être présent à la racine du projet,
afin de mettre à jour éventuellement les dépendances via pip. Par défaut, seuls
`mkdocs` et `mkdocs-material` sont installés.

Cependant, si vous mettez à jour les dépendances, vous devrez redémarrer (pas reconstruire)
l'image Docker.
